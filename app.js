const { argv } = require('./config/yargs.js');
const colors = require('colors');
const { createFile, listTable } = require('./multiply/multiply.js');

let command = argv._[0];

switch (command) {
    case 'list':
        listTable(argv.base, argv.limit)
        break;
    case 'create':
        createFile(argv.base, argv.limit)
            .then(file => console.log(`Created file: ${ file.green }`))
            .catch(e => console.log(e));
        break;
    default:
        console.log('Command not recognized');
        break;
};